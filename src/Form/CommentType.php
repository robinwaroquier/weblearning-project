<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            //->add('createdAt')
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr'  => [
                    "placeholder" => 'Titre du commentaire'
                ]
            ])
            ->add('rating', IntegerType::class, [
                'label' => 'Note',
                'attr'  => [
                    "placeholder" => 'Évaluation du cours'
                ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Commentaire',
                'attr'  => [
                    "placeholder" => 'Contenu du commentaire'
                ]
            ])
            //->add('author')
            //->add('course')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}