<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    #[Route('/team', name: 'team')]
    public function team(UserRepository $repository): Response
    {
        $team = $repository->findBy(
            [],
            [
                'roles' => 'ASC',
                'lastName' => 'DESC',
            ],
        );

        return $this->render('team/team.html.twig', [
            'team'  => $team
        ]);
    }
}
