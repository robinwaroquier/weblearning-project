<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCourseController extends AbstractController
{
    /**
     * @param CourseRepository $repository
     * @return Response
     */
    #[Route('/admin/courses', name: 'admin_courses')]
    public function adminCourses(CourseRepository $repository): Response
    {
        $courses = $repository->findBy(
            [],
            ['createdAt'    => 'DESC']
        );

        return $this->render('admin/courses/courses.html.twig', [
            'courses'   => $courses,
        ]);
    }

    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    #[Route('/admin/viewcourse/{id}', name: 'admin_view_course')]
    public function viewCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $course->setIsPublished(!$course->getIsPublished());
        $manager->flush();

        return $this->redirectToRoute('admin_courses');
    }

    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/delcourse/{id}', name: 'admin_del_course')]
    public function delCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $manager->remove($course);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le cours "' .$course->getName() .'" a bien été supprimé!'
        );

        return $this->redirectToRoute('admin_courses');
    }

    #[Route('/admin/newcourse', name: 'admin_new_course')]
    public function newCourse(EntityManagerInterface $manager, Request $request)
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $course->setSlug($slug->slugify($course->getName()));
            $course->setCreatedAt(new \DateTimeImmutable());
            $course->setIsPublished(true);
            $manager->persist($course);
            $manager->flush();
            $this->addFlash(
                'success',
                'La formation ' .$course->getName() .' a bien été ajoutée !'
            );
            return $this->redirectToRoute('admin_courses');
        }

        return $this->renderForm('admin/courses/newcourse.html.twig', [
            'form' => $form
        ]);
    }

}