<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    /**
     * @param UserRepository $repository
     * @return Response
     */
    #[Route('/admin/users', name: 'admin_users')]
    public function adminUsers(UserRepository $repository): Response
    {
        $users = $repository->findBy(
            [],
            ['createdAt'    => 'DESC']
        );

        return $this->render('admin/users/users.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('admin/viewuser/{id}', name: 'admin_view_user')]
    public function viewUsers(User $user, EntityManagerInterface $manager): Response
    {
        $user->setIsDisabled(!$user->getIsDisabled());
        $manager->flush();

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @param User $user
     * @param EntityManagerInterface $manager
     * @param $role
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    #[Route('admin/user-promote/{id}/{role}', name: 'admin_user_promote')]
    public function promote(User $user, EntityManagerInterface $manager, $role)
    {
        $user->setRoles([$role]);
        $manager->flush();
        return $this->redirectToRoute('admin_users');
    }

}
