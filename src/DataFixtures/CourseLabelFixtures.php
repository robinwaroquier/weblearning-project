<?php

namespace App\DataFixtures;

use App\Entity\CourseLevel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CourseLabelFixtures extends Fixture
{
        private array $levels = [
            'Débutant'      => 'Certificat de base',
            'Confirmé'      => 'Connaissance de base',
            'Spécialisé'    => 'Connaissance avancée',
            'Expert'        => 'Pratique professionnelle et expertise'
    ];

        public function load(ObjectManager $manager): void
    {
        foreach($this->levels as $title => $prerequised) {
        $level = new CourseLevel();
        $level->setName($title);
        $level->setPrerequisite($prerequised);
        $manager->persist($level);
        }
        $manager->flush();
    }

}
